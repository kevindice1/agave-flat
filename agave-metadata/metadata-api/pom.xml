<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.iplantc.agave</groupId>
		<artifactId>metadata</artifactId>
		<version>2.2.23</version>
	</parent>
	<artifactId>metadata-api</artifactId>
	<name>iPlant Agave API Metadata Service API Endpoints</name>
	<description>Service REST endpoints for the Agave Metadata service </description>
	<packaging>war</packaging>

	<scm>
		<connection>scm:git:ssh://git@bitbucket.org/agaveapi/agave-metadata.git</connection>
		<developerConnection>scm:git:ssh://git@bitbucket.org/agaveapi/agave-metadata.git</developerConnection>
		<url>https://bitbucket.org/agaveapi/agave-metadata</url>
	</scm>

	<properties>
		<force.check.update>false</force.check.update>
		<service.war.name>meta</service.war.name>
		<hibernate.hbm2ddl>update</hibernate.hbm2ddl>
	</properties>

	<build>
		<finalName>${service.war.name}</finalName>
		<sourceDirectory>src/main/java</sourceDirectory>

		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<filtering>true</filtering>
			</resource>
		</resources>
		<testResources>
			<testResource>
				<directory>src/test/resources</directory>
				<filtering>true</filtering>
			</testResource>
		</testResources>
		<plugins>
			<!-- ************************************ -->
			<!-- *** WAR BUILDER ** -->
			<!-- ************************************ -->

			<!-- Build war here and replace all the properties in the config file.
				pick the desired profile above or specify it at the command line to select
				the appropriate set of values -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<executions>
					<execution>
						<id>prepare-war</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>war</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<warName>${service.war.name}</warName>
					<useCache>true</useCache>
					<nonFilteredFileExtensions>
						<nonFilteredFileExtension>jar</nonFilteredFileExtension>
						<nonFilteredFileExtension>flt</nonFilteredFileExtension>
					</nonFilteredFileExtensions>
					<packagingExcludes>
						WEB-INF/lib/servlet-api-*.jar
					</packagingExcludes>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
						<manifestEntries>
							<Implementation-Build>$\{buildNumber}</Implementation-Build>
						</manifestEntries>
					</archive>
					<webResources>
						<resource>
							<directory>src/main/java</directory>
							<targetPath>WEB-INF/classes</targetPath>
							<filtering>true</filtering>
							<excludes>
								<exclude>**/*.java</exclude>
							</excludes>
						</resource>
						<resource>
							<directory>src/main/resources</directory>
							<targetPath>WEB-INF/classes</targetPath>
							<filtering>true</filtering>
							<includes>
								<include>service.properties</include>
								<include>hibernate.cfg.xml</include>
								<include>roles.properties</include>
							</includes>
						</resource>
						<resource>
							<directory>src/main/webapp/WEB-INF</directory>
							<targetPath>WEB-INF</targetPath>
							<filtering>true</filtering>
						</resource>
					</webResources>
				</configuration>
			</plugin>

			<!-- Surefire testng testing -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<suiteXmlFiles>
						<suiteXmlFile>src/test/resources/testng.xml</suiteXmlFile>
					</suiteXmlFiles>
				</configuration>
			</plugin>

			<!-- Don't deploy the war -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-install-plugin</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>

		</plugins>

		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>${org.eclipse.m2e.lifecycle-mapping.version}</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.codehaus.gmaven
										</groupId>
										<artifactId>
											gmaven-plugin
										</artifactId>
										<versionRange>
											${gmaven-plugin.versionRange}
										</versionRange>
										<goals>
											<goal>compile</goal>
											<goal>testCompile</goal>
											<goal>
												generateTestStubs
											</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<dependencies>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>metadata-core</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>common-legacy-api</artifactId>
			<version>${project.version}</version>
		</dependency>
	</dependencies>

</project>
